package com.cami.theatre.controllers;

import com.cami.theatre.dao.play.PlayDAOImpl;
import com.cami.theatre.dao.seat.SeatDAOImpl;
import com.cami.theatre.entity.Play;
import com.cami.theatre.entity.Seat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mortbay.jetty.webapp.WebAppContext;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.View;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebAppContext.class})
@WebAppConfiguration
@SpringBootTest
public class ReservationProcessControllerTest {

    @InjectMocks
    ReservationProcessController controller;

    @Mock
    SeatDAOImpl seatDAO;

    @Mock
    PlayDAOImpl playDAO;

    @Mock
    View mockView;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(controller)
                .setSingleView(mockView)
                .build();

        Seat seatNotReserved = new Seat("A1", 50, false, false);
        Seat seatReserved = new Seat("A2", 50, true, false);
        Play play = new Play("Test", "2018-12-12");
        doReturn(seatNotReserved).when(seatDAO).getSeatById(1);
        doReturn(seatReserved).when(seatDAO).getSeatById(2);

        when(seatDAO.listAllSeats()).thenReturn(Arrays.asList(seatNotReserved, seatReserved));
        when(playDAO.listAllPlays()).thenReturn(Arrays.asList(play));
        doNothing().when(seatDAO).reserve(anyList());
    }

    @Test
    public void checkSeatsEndpoint() throws Exception {

        mockMvc.perform(get("/seats"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("seats", "plays", "seatsAmount", "priceTotal"))
                .andExpect(model().attribute("seats", Collections.emptyList()))
                .andExpect(model().attribute("plays", Collections.emptyList()))
                .andExpect(model().attribute("seatsAmount", 0))
                .andExpect(model().attribute("priceTotal", 0))
                .andExpect(view().name("allSeats"));
    }

    @Test
    public void checkSuccessEndpoint() throws Exception {

        mockMvc.perform(get("/success"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("reservedSeats", "totalAmount"))
                .andExpect(model().size(2))
                .andExpect(view().name("success"));
    }

    @Test
    public void checkFinishEndpoint() throws Exception {

        mockMvc.perform(get("/finish"))
                .andExpect(status().is3xxRedirection());
    }


    @Test
    public void checkReservationCompleteEndpoint() throws Exception {

        mockMvc.perform(get("/reservationcomplete"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("reservedSeats", "totalAmount", "reservationNumber"))
                .andExpect(view().name("reservationComplete"));
    }

    @Test
    public void checkReservationFailureEndpoint() throws Exception {

        mockMvc.perform(get("/reservationfailure"))
                .andExpect(status().isOk())
                .andExpect(model().attributeDoesNotExist("reservedSeats", "totalAmount", "reservationNumber"))
                .andExpect(view().name("failure"));
    }

}
