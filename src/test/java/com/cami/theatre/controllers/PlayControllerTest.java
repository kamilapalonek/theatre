package com.cami.theatre.controllers;


import com.cami.theatre.dao.play.PlayDAOImpl;
import com.cami.theatre.entity.Play;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mortbay.jetty.webapp.WebAppContext;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.View;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebAppContext.class})
@WebAppConfiguration
@SpringBootTest
public class PlayControllerTest {

    @InjectMocks
    PlayController controller;

    @Mock
    PlayDAOImpl playDAO;

    @Mock
    View mockView;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(controller)
                .setSingleView(mockView)
                .build();

        Play play = new Play("Test", "2019-03-03");

        when(playDAO.listAllPlays()).thenReturn(Arrays.asList(play));
    }

    @Test
    public void checkSeatsEndpoint() throws Exception {

        mockMvc.perform(get("/plays"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("plays"))
                .andExpect(model().attribute("plays", Collections.emptyList()))
                .andExpect(view().name("playView"));
    }
}