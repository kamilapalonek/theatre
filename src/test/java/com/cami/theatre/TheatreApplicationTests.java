package com.cami.theatre;

import com.cami.theatre.controllers.PlayControllerTest;
import com.cami.theatre.controllers.ReservationProcessControllerTest;
import com.cami.theatre.controllers.WelcomeControllerTest;
import com.cami.theatre.dao.PlayDAOTest;
import com.cami.theatre.dao.SeatDAOTest;
import com.cami.theatre.dao.UserDAOTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
		UserDAOTest.class,
		SeatDAOTest.class,
		PlayDAOTest.class,
		ReservationProcessControllerTest.class,
		WelcomeControllerTest.class,
		PlayControllerTest.class
})
public class TheatreApplicationTests {

	@Test
	public void contextLoads() {
	}

}

