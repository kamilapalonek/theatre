package com.cami.theatre.dao;

import com.cami.theatre.dao.play.PlayDAOImpl;
import com.cami.theatre.entity.Play;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayDAOTest {

    @Mock
    private PlayDAOImpl playDAO;

    @Before
    public void before() {
        Play firstPlay = new Play("Test 1", "2018-12-12");
        Play secondPlay = new Play("Test 2", "2019-01-01");
        when(playDAO.listAllPlays()).thenReturn(Arrays.asList(firstPlay, secondPlay));
        when(playDAO.getPlayById(1)).thenReturn(firstPlay);
        when(playDAO.getPlayById(2)).thenReturn(secondPlay);
    }

    @Test
    public void checkValuesForPlays() {
        assertEquals("Test 1", playDAO.getPlayById(1).getTitle());
        assertEquals("2018-12-12", playDAO.getPlayById(1).getDate());

        assertEquals("Test 2", playDAO.getPlayById(2).getTitle());
        assertEquals("2019-01-01", playDAO.getPlayById(2).getDate());

    }

}

