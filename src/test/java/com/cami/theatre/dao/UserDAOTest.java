package com.cami.theatre.dao;

import com.cami.theatre.dao.user.UserDAOImpl;
import com.cami.theatre.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDAOTest {

    @Mock
    private UserDAOImpl userDAO;

    @Before
    public void before() {
        User defaultUser = new User("test@email.com", "password");
        when(userDAO.logIn(defaultUser.getEmail(), defaultUser.getPassword())).thenReturn(true);
    }

    @Test
    public void shouldReturnTrueWhenProvidingCorrectUserData() {
        assertTrue(userDAO.logIn("test@email.com", "password"));
    }


    @Test
    public void shouldReturnFalseWhenProvidingIncorrectUserData() {
        assertFalse(userDAO.logIn("test@email.com", "wrongpassword"));
        assertFalse(userDAO.logIn("wrong@email.com", "password"));
    }

}
