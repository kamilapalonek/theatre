package com.cami.theatre.dao;

import com.cami.theatre.dao.seat.SeatDAOImpl;
import com.cami.theatre.entity.Seat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SeatDAOTest {

    @Mock
    SeatDAOImpl seatDAO;

    @Before
    public void before() {
        Seat seatNotReserved = new Seat("A1", 50, false, false);
        Seat seatReserved = new Seat("A2", 50, true, false);
        when(seatDAO.getSeatById(1)).thenReturn(seatNotReserved);
        when(seatDAO.getSeatById(2)).thenReturn(seatReserved);
        when(seatDAO.listAllSeats()).thenReturn(Arrays.asList(seatNotReserved, seatReserved));
    }


    @Test
    public void checkListAllSeats() {
        assertEquals(2, seatDAO.listAllSeats().size());
    }

    @Test
    public void checkGetSeatsById() {
        assertEquals("A1", seatDAO.getSeatById(1).getSeatNumber());
        assertEquals("A2", seatDAO.getSeatById(2).getSeatNumber());
    }

}
