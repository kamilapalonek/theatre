const handleLogin = () => {
    let email = document.forms["loginForm"]["email"].value;
    let password = document.forms["loginForm"]["password"].value;

    if (email === "" || email == null) {
        alert('Email is empty')
        return false
    }

    if (password.length < 4 || password === "" || password == null) {
        alert('Password is too short or is empty')
        return false
    }
}

const logIn = () => {
    window.location = '/login'
}

const confirmLogOut = () => {
    const answer = confirm('Are you sure you wanna log out? All changes will be lost.')
    if (answer) {
        window.location = "/finish"
    }
}