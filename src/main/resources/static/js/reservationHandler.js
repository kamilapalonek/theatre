const addReserved = id => {
    $(`#${id}`).toggleClass('reserved')
    window.location.href = '/seat/' + id
}

const reservationSuccess = () => {
    window.location = '/success'
}

const reservationFailure = () => {
    window.location = '/finish'
}


const goBack = () => {
    window.location = '/finish'
}

const popUp = () => {
    const answer = confirm('Are you sure?')
    if (answer) {
        window.location = '/play' + id;
    }
}
