CREATE DATABASE  IF NOT EXISTS `theatre`;
USE `theatre`;

DROP TABLE IF EXISTS `seat`;

CREATE TABLE `seat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seat_number` varchar(45) DEFAULT NULL,
  `price` INTEGER(3) DEFAULT 0,
  `is_reserved` bit DEFAULT 0,
  `to_be_reserved` bit DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `play`;
CREATE TABLE `play` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


INSERT INTO `seat` VALUES
                          (1,'A1', 50, 0, DEFAULT),
                          (2,'A2', 50, 0, DEFAULT),
                          (3,'A3', 70, 0, DEFAULT),
                          (4,'A4', 70, 0, DEFAULT),
                          (5,'A5', 50, 1, DEFAULT),
                          (6,'B1', 40, 0, DEFAULT),
                          (7,'B2', 40, 1, DEFAULT),
                          (8,'B3', 50, 1, DEFAULT),
                          (9,'B4', 50, 1, DEFAULT),
                          (10,'B5', 40, 0, DEFAULT),
                          (11,'C1', 40, 0, DEFAULT),
                          (12,'C2', 40, 0, DEFAULT),
                          (13,'C3', 50, 0, DEFAULT),
                          (14,'C4', 50, 0, DEFAULT),
                          (15,'C5', 40, 1, DEFAULT),
                          (16,'D1', 40, 0, DEFAULT),
                          (17,'D2', 40, 0, DEFAULT),
                          (18,'D3', 50, 1, DEFAULT),
                          (19,'D4', 50, 1, DEFAULT),
                          (20,'D5', 40, 1, DEFAULT),
                          (21,'E1', 40, 0, DEFAULT),
                          (22,'E2', 40, 0, DEFAULT),
                          (23,'E3', 40, 0, DEFAULT),
                          (24,'E4', 40, 0, DEFAULT),
                          (25,'E5', 40, 0, DEFAULT),
                          (26,'F1', 40, 0, DEFAULT),
                          (27,'F2', 40, 0, DEFAULT),
                          (28,'F3', 40, 0, DEFAULT),
                          (29,'F4', 40, 0, DEFAULT),
                          (30,'F5', 40, 0, DEFAULT);


INSERT INTO `play` VALUES
                        (1, "Other People's Heartache", "2018-01-12 18:30"),
                        (2, "Other People's Heartache", "2018-01-14 18:30"),
                        (3, "Other People's Heartache", "2018-01-14 18:30");


INSERT INTO `user` VALUES
(1, 'cami@gmail.com', 'cami')