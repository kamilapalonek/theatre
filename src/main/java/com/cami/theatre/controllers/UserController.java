package com.cami.theatre.controllers;

import com.cami.theatre.dao.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class UserController {
    private final UserDAO userDAO;

    @Autowired
    public UserController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @RequestMapping(value = "/login")
    public String logIn() throws IOException {
        return "login";
    }

    @RequestMapping(value = "/verify")
    public void verify(@RequestParam("email") String email, @RequestParam("password") String password, HttpServletResponse response) throws IOException {

        if (userDAO.logIn(email, password)) {
            response.sendRedirect("/seats");
        } else {
            response.sendRedirect("/error");
        }
    }


}
