package com.cami.theatre.controllers;


import com.cami.theatre.dao.play.PlayDAO;
import com.cami.theatre.dao.seat.SeatDAO;
import com.cami.theatre.entity.Play;
import com.cami.theatre.entity.Seat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.cami.theatre.dao.seat.SeatDAOImpl.*;

@Controller
public class ReservationProcessController {

    private final SeatDAO seatDAO;
    private final PlayDAO playDAO;

    @Autowired
    public ReservationProcessController(SeatDAO seatDAO, PlayDAO playDAO) {
        this.seatDAO = seatDAO;
        this.playDAO = playDAO;
    }

    @RequestMapping("/seats")
    public String listAllSeats(Model model) {
        List<Seat> seats = seatDAO.listAllSeats();
        List<Play> plays = playDAO.listAllPlays();

        int seatsAmount = getReservedSeats().size();
        int priceTotal = getTotalAmount();

        model.addAttribute("seats", seats);
        model.addAttribute("plays", plays);
        model.addAttribute("seatsAmount", seatsAmount);
        model.addAttribute("priceTotal", priceTotal);

        return "allSeats";
    }

    @RequestMapping(value = "/seat/{id}")
    public void reserveSeat(@PathVariable("id") int id, HttpServletResponse response) throws IOException {
        Seat chosenSeat = seatDAO.getSeatById(id);
        if (chosenSeat.isToBeReserved()) {
            chosenSeat.setToBeReserved(false);
            seatDAO.unreserveSeat(id);
        } else {
            seatDAO.reserveSeat(id);
        }
        response.sendRedirect("/seats");
    }

    @RequestMapping("/success")
    public String success(Model model) {
        List<Seat> reservedSeats = getReservedSeats();
        int totalAmount = getTotalAmount();

        seatDAO.reserve(reservedSeats);

        model.addAttribute("reservedSeats", reservedSeats);
        model.addAttribute("totalAmount", totalAmount);

        return "success";
    }

    @RequestMapping("/finish")
    public void finish(HttpServletResponse response) throws IOException {
        setReservedSeats(new ArrayList<>());
        setTotalAmount(0);

        response.sendRedirect("/");
    }

    @RequestMapping("/reservationcomplete")
    public String reservationComplete(Model model) {
        List<Seat> reservedSeats = getReservedSeats();
        int totalAmount = getTotalAmount();
        String reservationNumber = UUID.randomUUID().toString();
        model.addAttribute("reservedSeats", reservedSeats);
        model.addAttribute("totalAmount", totalAmount);
        model.addAttribute("reservationNumber", reservationNumber);
        return "reservationComplete";
    }

    @RequestMapping("/reservationfailure")
    public String failure(HttpServletResponse response) throws IOException {
        setReservedSeats(new ArrayList<>());
        setTotalAmount(0);
        return "failure";
    }
}
