package com.cami.theatre.controllers;

import com.cami.theatre.dao.play.PlayDAO;
import com.cami.theatre.entity.Play;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class PlayController {

    private final PlayDAO playDAO;

    @Autowired
    public PlayController(PlayDAO playDAO) {
        this.playDAO = playDAO;
    }

    @RequestMapping("/plays")
    public String listAllPlays(Model model) {
        List<Play> plays = playDAO.listAllPlays();
        model.addAttribute("plays", plays);
        return "playView";
    }

    @RequestMapping(value = "/play/{id}")
    public void chooseDate(@PathVariable("id") int id, Model model, HttpServletResponse response) throws IOException {
        response.sendRedirect("/seats");
    }
}
