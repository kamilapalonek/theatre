package com.cami.theatre.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="seat")
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @Column(name="seat_number")
    private String seatNumber;

    @Column(name="price")
    private int price;

    @Column(name="is_reserved")
    private boolean reserved;

    @Column(name = "to_be_reserved")
    private boolean toBeReserved;


    public Seat() {
    }

    public Seat(String seatNumber, int price, boolean reserved, boolean toBeReserved) {
        this.seatNumber = seatNumber;
        this.price = price;
        this.reserved = reserved;
        this.toBeReserved = toBeReserved;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean getReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isReserved() {
        return reserved;
    }

    public boolean isToBeReserved() {
        return toBeReserved;
    }

    public void setToBeReserved(boolean toBeReserved) {
        this.toBeReserved = toBeReserved;
    }

    @Override
    public String toString() {
        return "Seat{" +
                "id=" + id +
                ", seatNumber='" + seatNumber + '\'' +
                ", price=" + price +
                ", reserved=" + reserved +
                ", toBeReserved=" + toBeReserved +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seat seat = (Seat) o;
        return getId() == seat.getId() &&
                getPrice() == seat.getPrice() &&
                isReserved() == seat.isReserved() &&
                isToBeReserved() == seat.isToBeReserved() &&
                Objects.equals(getSeatNumber(), seat.getSeatNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSeatNumber(), getPrice(), isReserved(), isToBeReserved());
    }
}
