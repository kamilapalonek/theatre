package com.cami.theatre.dao.play;

import com.cami.theatre.entity.Play;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class PlayDAOImpl implements PlayDAO{

    private final EntityManager entityManager;

    @Autowired
    public PlayDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public List<Play> listAllPlays() {
        Session currentSession = entityManager.unwrap(Session.class);

        Query<Play> theQuery = currentSession.createQuery(
                "from Play", Play.class
        );

        return theQuery.getResultList();
    }

    @Override
    public Play getPlayById(int id) {
        Session currentSession = entityManager.unwrap(Session.class);

        return currentSession.get(Play.class, id);
    }
}
