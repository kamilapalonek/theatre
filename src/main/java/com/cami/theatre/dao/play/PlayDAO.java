package com.cami.theatre.dao.play;

import com.cami.theatre.entity.Play;

import java.util.List;

public interface PlayDAO {
    public List<Play> listAllPlays();

    public Play getPlayById(int id);
}
