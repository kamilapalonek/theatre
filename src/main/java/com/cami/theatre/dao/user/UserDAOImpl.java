package com.cami.theatre.dao.user;

import com.cami.theatre.entity.User;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    private final EntityManager entityManager;

    @Autowired
    public UserDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public boolean logIn(String email, String password) {
        Session currentSession = entityManager.unwrap(Session.class);

        Query<User> theQuery = currentSession.createQuery(
                "from User", User.class
        );

        List<User> list = theQuery.list();

        User user = list.get(0);
        return user.getEmail().compareTo(email) == 0 && user.getPassword().compareTo(password) == 0;
    }

    public User findUserByEmail(String email) {
        Session currentSession = entityManager.unwrap(Session.class);

        Query<User> theQuery = currentSession.createQuery(
                "from User", User.class
        );

        List<User> list = theQuery.list();

        User user = list.get(0);
        if (user.getEmail().equals(email)) {
            return user;
        }
        return null;
    }
}
