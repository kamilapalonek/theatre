package com.cami.theatre.dao.user;

import com.cami.theatre.entity.User;

public interface UserDAO {

    public boolean logIn(String email, String password);

    public User findUserByEmail(String email);
}
