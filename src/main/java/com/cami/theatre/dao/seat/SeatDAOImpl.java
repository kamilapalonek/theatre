package com.cami.theatre.dao.seat;

import com.cami.theatre.entity.Seat;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SeatDAOImpl implements SeatDAO {

    private static List<Seat> reservedSeats = new ArrayList<>();
    private static int totalAmount = 0;
    private final EntityManager entityManager;

    @Autowired
    public SeatDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public static List<Seat> getReservedSeats() {
        return reservedSeats;
    }

    public static void setReservedSeats(List<Seat> reservedSeats) {
        SeatDAOImpl.reservedSeats = reservedSeats;
    }

    public static int getTotalAmount() {
        return totalAmount;
    }

    public static void setTotalAmount(int totalAmount) {
        SeatDAOImpl.totalAmount = totalAmount;
    }

    @Override
    @Transactional
    public List<Seat> listAllSeats() {
        Session currentSession = entityManager.unwrap(Session.class);

        Query<Seat> theQuery = currentSession.createQuery(
                "from Seat", Seat.class
        );

        return theQuery.getResultList();
    }

    @Override
    @Transactional
    public void reserveSeat(int id) {
        Session currentSession = entityManager.unwrap(Session.class);
        Seat seat = currentSession.get(Seat.class, id);
        totalAmount += seat.getPrice();
        if (!seat.isToBeReserved()) {
            seat.setToBeReserved(true);
            reservedSeats.add(seat);
        } else {
            reservedSeats.remove(seat);
        }
    }

    @Override
    @Transactional
    public void reserve(List<Seat> seats) {
        Session currentSession = entityManager.unwrap(Session.class);
        for (Seat seat : reservedSeats) {
            seat.setReserved(true);

            currentSession.saveOrUpdate(seat);
        }
    }

    @Override
    @Transactional
    public void unreserveSeat(int id) {
        Session currentSession = entityManager.unwrap(Session.class);

        Seat seat = currentSession.get(Seat.class, id);
        if (totalAmount != 0) {
            totalAmount -= seat.getPrice();
        }
        reservedSeats.forEach(s -> {
                    if (seat.getId() == s.getId()) {
                        s.setToBeReserved(false);
                    }
                }
        );
        reservedSeats.remove(seat);
    }


    @Override
    public Seat getSeatById(int id) {
        Session currentSession = entityManager.unwrap(Session.class);

        return currentSession.get(Seat.class, id);
    }

}
