package com.cami.theatre.dao.seat;

import com.cami.theatre.entity.Seat;

import java.util.List;

public interface SeatDAO {

    public List<Seat> listAllSeats();

    public void reserveSeat(int id);

    public void unreserveSeat(int id);

    public void reserve(List<Seat> seats);

    public Seat getSeatById(int id);
}
